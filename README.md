# DLV Sublime 2 Package

DLV syntax highlighting (tmLanguage) for Sublime 2 (and Textmate presumedly).

For more info about DLV and Answer Set Programming in general please see the
[official website][1].


## How to Use

Install the .tmLanguage file by dropping it into the "User" package in the Packages
directory of your Sublime 2 installation. You can also go ahead and check out this
repository into the Packages directory. For more information on Sublime 2 packages in
general see the [Sublime 2 Documentation][2].

In the editor use View > Syntax > DLV to explicity highlight the current file as a 
DLV source. You don't need to do this explicitly when opening .dl files.


## Completeness

So far I have only added patterns for the most common parts of the DLV language
(and comparable ones such as Potassco's format). This includes highlighting for
rules of Extended Logic Programs, disjunctive rules, built-in predicates such as #num,
anonymous variables and of course comments.

Please note that terms currently are currently not matched any different than
predicate names, as matching complex terms is far from trivial using (extended)
regular expressions.

There also is a simple build system that tries to run dlv from $PATH with the -silent
option. As this probably isn't especially useful for any more involved work, I
recommend setting up a project and adding specific build configurations to the
project settings file such as these:

```json
{
  "folders":
  [
    {
      "path": "/home/user/project/"
    }
  ],

  "build_systems":
  [
    {
      "name": "evaluate check",
      "cmd": ["dlv", "-pfilter=assigned,person", "-silent", "$project_path/check.dl", "$file"]
    },
    {
      "name": "evaluate guess",
      "cmd": ["dlv", "-pfilter=assigned", "-silent", "$project_path/guess.dl", "$file"]
    }
  ]
}
```


[1]: http://www.dlvsystem.com/
[2]: http://docs.sublimetext.info/en/latest/extensibility/packages.html#installation-of-packages